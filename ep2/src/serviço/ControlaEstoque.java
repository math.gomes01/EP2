package servi�o;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class ControlaEstoque {
	public static boolean validaEstoque(String valor, String produto, String qtd) {
		try {
			if (produto.isEmpty() || valor.isEmpty() || qtd.isEmpty()) {
				JOptionPane.showMessageDialog(null, "campo invalido");
				return false;
			}
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(null, "campo invalido");
			return false;
		}
		return true;
	}

	public static void gravaEstoque(String valor, String produto, String qtd) throws IOException {

		File file = new File("estoque.txt");
		BufferedWriter output = null;
		try {

			output = new BufferedWriter(new FileWriter(file, true));
			output.write(valor);
			output.write(",");
			output.write(produto);
			output.write(",");
			output.write(qtd);
			output.newLine();

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o produto");
		} finally {
			if (output != null) {
				output.close();
			}
		}
	}

	public static boolean leEstoque(String arquivo, String produto) throws IOException {
		Scanner in = new Scanner(new FileReader(arquivo));
		int aux = 0;
		while (in.hasNextLine()) {
			String estoque = null;
			String line = in.nextLine();
			String[] array = line.trim().split(",");
			estoque = array[1].trim();
			if (estoque.equals(produto)) {
				if (Integer.parseInt(array[2]) <= 5) {
					JOptionPane.showMessageDialog(null,
							"Produto " + produto + " com baixa quantidade no estoque \n\t\t Repor!");
					return false;
				} else {
					aux = 0;
					return true;
				}
			} else
				aux = 1;
		}
		in.close();
		if (aux != 0) {
			JOptionPane.showMessageDialog(null, "produto " + produto + " nao esta no estoque");
			return false;
		} else
			return true;
	}

	public static Double leValor(String arquivo, String produto, String produto1, String produto2) throws IOException {
		Scanner in = new Scanner(new FileReader(arquivo));
		Double valor = 0.00;
		while (in.hasNextLine()) {
			String estoque = null;
			String line = in.nextLine();
			String[] array = line.trim().split(",");
			estoque = array[1].trim();
			if (estoque.equals(produto) || estoque.equals(produto1) || estoque.equals(produto2)) {
				valor += Double.parseDouble(array[0]);
			}
		}
		return valor;
	}
}

package principal;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import servi�o.ControlaEmpregado;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Toolkit;

public class FrameEmpregado extends JFrame {

	private JPanel contentPane;
	private JTextField textoNumID;
	private JTextField textoNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameEmpregado frame = new FrameEmpregado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameEmpregado() {
		setForeground(Color.WHITE);
		setBackground(Color.WHITE);
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\mathg_000\\Documents\\matheus\\OO\\TrabalhoEP2\\EP2\\ep2\\src\\unnamed.png"));
		setTitle("To com fome, quero mais");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 281, 158);
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] {70, 96, 0, 0};
		gbl_contentPane.rowHeights = new int[] {0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblEmpregado = new JLabel("Empregado");
		GridBagConstraints gbc_lblEmpregado = new GridBagConstraints();
		gbc_lblEmpregado.anchor = GridBagConstraints.WEST;
		gbc_lblEmpregado.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmpregado.gridx = 1;
		gbc_lblEmpregado.gridy = 0;
		contentPane.add(lblEmpregado, gbc_lblEmpregado);
		
		JLabel lblNewLabel = new JLabel("Numero de ID : ");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		textoNumID = new JTextField();
		GridBagConstraints txtNumID = new GridBagConstraints();
		txtNumID.anchor = GridBagConstraints.WEST;
		txtNumID.insets = new Insets(0, 0, 5, 5);
		txtNumID.gridx = 1;
		txtNumID.gridy = 1;
		contentPane.add(textoNumID, txtNumID);
		textoNumID.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome : ");
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.anchor = GridBagConstraints.EAST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 0;
		gbc_lblNome.gridy = 2;
		contentPane.add(lblNome, gbc_lblNome);
		
		textoNome = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.WEST;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		contentPane.add(textoNome, gbc_textField);
		textoNome.setColumns(10);
		
		JButton botaoProdto = new JButton("Produtos");
		botaoProdto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand() != null){
					ControlaEmpregado evento = new ControlaEmpregado();
					if(!evento.validaEmpregado(textoNome.getText(), textoNumID.getText())){
						textoNome.setText(null);
						textoNumID.setText(null);
						FrameEmpregado.main(null);
						FrameEmpregado.this.dispose();
					}
					else{
						evento.setEmpregado(textoNumID.getText(), textoNome.getText());
						FrameEstoque.main(null);
						FrameEmpregado.this.dispose();
					}
				}
			}
		});
		
		JButton btnNewButton = new JButton("Pedido");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand() != null){
					ControlaEmpregado evento = new ControlaEmpregado();
					if(!evento.validaEmpregado(textoNome.getText(), textoNumID.getText())){
						textoNome.setText(null);
						textoNumID.setText(null);
						FrameEmpregado.main(null);
						FrameEmpregado.this.dispose();
					}
					else{
						evento.setEmpregado(textoNumID.getText(), textoNome.getText());
						FramePedido.main(null);
						FrameEmpregado.this.dispose();
					}
				}
			}
		});
		GridBagConstraints gbc_btnNewButton1 = new GridBagConstraints();
		gbc_btnNewButton1.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton1.gridx = 0;
		gbc_btnNewButton1.gridy = 3;
		contentPane.add(btnNewButton, gbc_btnNewButton1);
		
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 3;
		contentPane.add(botaoProdto, gbc_btnNewButton);
	}

}

package principal;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dados.Lista;
import servi�o.ControlaEstoque;
import servi�o.ControlaPedido;

import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class FramePedido extends JFrame {

	private JPanel contentPane;
	private JTextField textoBebida;
	private JTextField textoComida;
	private JTextField textoSobremessa;
	private JLabel lblFormaDePagamento;
	private JButton botaoOK;
	private JButton botaoCartao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FramePedido frame = new FramePedido();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FramePedido() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\mathg_000\\Documents\\matheus\\OO\\TrabalhoEP2\\EP2\\ep2\\src\\unnamed.png"));
		setForeground(Color.WHITE);
		setBackground(Color.WHITE);
		setTitle("To com fome, quero mais");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 287, 196);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 96, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblPedido = new JLabel("PEDIDO");
		GridBagConstraints gbc_lblPedido = new GridBagConstraints();
		gbc_lblPedido.insets = new Insets(0, 0, 5, 5);
		gbc_lblPedido.gridx = 3;
		gbc_lblPedido.gridy = 2;
		contentPane.add(lblPedido, gbc_lblPedido);

		JLabel lblBebida = new JLabel("Bebida : ");
		GridBagConstraints gbc_lblBebida = new GridBagConstraints();
		gbc_lblBebida.insets = new Insets(0, 0, 5, 5);
		gbc_lblBebida.gridx = 2;
		gbc_lblBebida.gridy = 3;
		contentPane.add(lblBebida, gbc_lblBebida);

		textoBebida = new JTextField();
		GridBagConstraints txtBebida = new GridBagConstraints();
		txtBebida.insets = new Insets(0, 0, 5, 5);
		txtBebida.fill = GridBagConstraints.HORIZONTAL;
		txtBebida.gridx = 3;
		txtBebida.gridy = 3;
		contentPane.add(textoBebida, txtBebida);
		textoBebida.setColumns(10);

		JLabel lblComida = new JLabel("Comida : ");
		GridBagConstraints gbc_lblComida = new GridBagConstraints();
		gbc_lblComida.insets = new Insets(0, 0, 5, 5);
		gbc_lblComida.gridx = 2;
		gbc_lblComida.gridy = 4;
		contentPane.add(lblComida, gbc_lblComida);

		textoComida = new JTextField();
		GridBagConstraints txtComida = new GridBagConstraints();
		txtComida.insets = new Insets(0, 0, 5, 5);
		txtComida.fill = GridBagConstraints.HORIZONTAL;
		txtComida.gridx = 3;
		txtComida.gridy = 4;
		contentPane.add(textoComida, txtComida);
		textoComida.setColumns(10);

		JLabel lblSobremessa = new JLabel("Sobremessa :");
		GridBagConstraints gbc_lblSobremessa = new GridBagConstraints();
		gbc_lblSobremessa.insets = new Insets(0, 0, 5, 5);
		gbc_lblSobremessa.gridx = 2;
		gbc_lblSobremessa.gridy = 5;
		contentPane.add(lblSobremessa, gbc_lblSobremessa);

		textoSobremessa = new JTextField();
		GridBagConstraints txtSobremessa = new GridBagConstraints();
		txtSobremessa.insets = new Insets(0, 0, 5, 5);
		txtSobremessa.fill = GridBagConstraints.HORIZONTAL;
		txtSobremessa.gridx = 3;
		txtSobremessa.gridy = 5;
		contentPane.add(textoSobremessa, txtSobremessa);
		textoSobremessa.setColumns(10);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (arg0.getActionCommand() != null) {
					ControlaPedido pedido = new ControlaPedido();
					if (!pedido.validaPedido(textoBebida.getText(), textoComida.getText(), textoSobremessa.getText())) {
						textoBebida.setText(null);
						textoComida.setText(null);
						textoSobremessa.setText(null);
						FramePedido.main(null);
						FramePedido.this.dispose();
					} else {
						try {
							if(ControlaEstoque.leEstoque("estoque.txt", textoBebida.getText()))
								pedido.setPedido(textoBebida.getText(), textoComida.getText(), textoSobremessa.getText());
							else{
								textoBebida.setText(null);
								textoComida.setText(null);
								textoSobremessa.setText(null);
								FramePedido.main(null);
								FramePedido.this.dispose();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						try {
							if(ControlaEstoque.leEstoque("estoque.txt", textoComida.getText()))
							pedido.setPedido(textoBebida.getText(), textoComida.getText(), textoSobremessa.getText());
							else{
								textoBebida.setText(null);
								textoComida.setText(null);
								textoSobremessa.setText(null);
								FramePedido.main(null);
								FramePedido.this.dispose();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						try {
							if(ControlaEstoque.leEstoque("estoque.txt", textoSobremessa.getText()))
								pedido.setPedido(textoBebida.getText(), textoComida.getText(), textoSobremessa.getText());
							else{
								textoBebida.setText(null);
								textoComida.setText(null);
								textoSobremessa.setText(null);
								FramePedido.main(null);
								FramePedido.this.dispose();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						if(JOptionPane.showConfirmDialog(null, "Deseja fazer mais algum pedido ?", null, JOptionPane.YES_NO_OPTION) == 1){
							try {
								FramePagamento.main(null, ControlaEstoque.leValor("estoque.txt", textoBebida.getText(), textoComida.getText(), textoSobremessa.getText()));
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							FramePedido.this.dispose();
						}else{
							textoBebida.setText(null);
							textoComida.setText(null);
							textoSobremessa.setText(null);
							FramePedido.main(null);
							FramePedido.this.dispose();
						}
					}
				}
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 5, 5);
		gbc_btnOk.gridx = 3;
		gbc_btnOk.gridy = 7;
		contentPane.add(btnOk, gbc_btnOk);
	}

}

package dados;

public class Empregado extends Pessoa{
	private String numeroId;
	
	public Empregado(String nome, String id) {
		super(nome);
		this.numeroId = id;
	}

	public String getNumeroId() {
		return numeroId;
	}

	public void setNumeroId(String numeroId) {
		this.numeroId = numeroId;
	}

}

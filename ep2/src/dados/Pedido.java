package dados;

public class Pedido {
	private String numBebida;
	private String numComida;
	private String numSobremesa;
	private Double valor;

	public Pedido(String bebida, String comida, String sobremessa, Double leValor) {
		this.numBebida = bebida;
		this.numComida = comida;
		this.numSobremesa = sobremessa;
		this.valor = leValor;
	}

	public Pedido() {
		// TODO Auto-generated constructor stub
	}

	public String getNumBebida() {
		return numBebida;
	}

	public void setNumBebida(String numBebida) {
		this.numBebida = numBebida;
	}

	public String getNumComida() {
		return numComida;
	}

	public void setNumComida(String numComida) {
		this.numComida = numComida;
	}

	public String getNumSobremesa() {
		return numSobremesa;
	}

	public void setNumSobremesa(String numSobremesa) {
		this.numSobremesa = numSobremesa;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}
